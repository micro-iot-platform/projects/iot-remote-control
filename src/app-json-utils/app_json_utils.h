/*
 * File Name: app_json_utils.h
 * Description:
 *
 *  Created on: 25-May-2019
 *      Author: Noyel Seth
 */

#ifndef APP_JSON_UTILS_H_
#define APP_JSON_UTILS_H_

#include "../iot-remote-control/iot_remote_control.h"

/*
 * Parse Application Config's response Json String:
 * {
 * 	"ctrl_relay_stat":true
 * }
 *
 */

em_err parse_config_json(char *string, IOT_REMOTE_CONTROL_CONFIGURATION_DATA *resp) ;

#endif /* APP_JSON_UTILS_H_ */
