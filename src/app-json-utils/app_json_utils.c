/*
 * File Name: app_json_utils.c
 * Description:
 *
 *  Created on: 25-May-2019
 *      Author: Noyel Seth
 */

#include "app_json_utils.h"

#include "parson.h"
#include "core_utils.h"

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#include <string.h>
#include <stdlib.h>
#include <math.h>

#define TAG "app_json_utils"

/****************************************************************************************************************************/

/*
 * Parse Application Config's response Json String:
 * {
 * 	"ctrl_relay_1_stat":true
 * 	"ctrl_relay_2_stat":false
 * }
 *
 */

em_err parse_config_json(char *string, IOT_REMOTE_CONTROL_CONFIGURATION_DATA *resp) {
	em_err ret = EM_FAIL;

	if (NULL != string) {
		JSON_Value *root_value;
		JSON_Object *root_object;
		root_value = json_parse_string(string);

		if (json_value_get_type(root_value) != JSONObject) {
			EM_LOGE(TAG, "Config Respons JSON Value type not matched\r\n");
			json_value_free(root_value);
			return ret;
		}

		root_object = json_value_get_object(root_value);

		resp->ctrl_relay_stat = json_object_get_number(root_object, GET_VAR_NAME(resp->ctrl_relay_stat, "->"));
		resp->ctrl_buzzer_stat = json_object_get_number(root_object, GET_VAR_NAME(resp->ctrl_buzzer_stat, "->"));

		EM_LOGD(TAG, "RELAY: %d", resp->ctrl_relay_stat);
		EM_LOGD(TAG, "BUZZER: %d", resp->ctrl_buzzer_stat);

		ret = EM_OK;
		json_value_free(root_value);

	} else {
		ret = EM_ERR_INVALID_RESPONSE;
	}
	return ret;

}
