
#include "iot-remote-control/iot_remote_control.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{	
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "Starting application built on the Iquester Core Embedded Framework");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "==================================================================");

	EM_LOGI(TAG, "Calling iot_remote_control_init() in your_module.c in your-module directory ...");
	iot_remote_control_init();
	EM_LOGI(TAG, "Returned from iot_remote_control_init()");

	while(1){
		iot_remote_control_loop();
		//EM_LOGI(TAG, "Returned from iot_remote_control_loop()");
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
	}
}
