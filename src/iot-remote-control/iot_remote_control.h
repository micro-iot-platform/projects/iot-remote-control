/**
 *
 */

#ifndef IOT_REMOTE_CONTROL_H_
#define IOT_REMOTE_CONTROL_H_

#include "thing.h"
#include "emmate.h"

typedef struct{
	int ctrl_relay_stat;	// if status 'true' then relay ON else Off
	int ctrl_buzzer_stat;	// if status 'true' then buzzer ON else Off
}IOT_REMOTE_CONTROL_CONFIGURATION_DATA;

/**
 *
 * */
void iot_remote_control_init();

/**
 * */
void iot_remote_control_loop();

#endif	/* IOT_REMOTE_CONTROL_H_ */
