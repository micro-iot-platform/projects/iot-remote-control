/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "iot_remote_control.h"
#include "../app-json-utils/app_json_utils.h"

#include "gpio_helper_api.h"
#include "appconfig_helper.h"
#include "event_group_core.h"

#define TAG	"iot_remote_control"

// IoT Remote Control Event Bits
// Relay Event Bits
#define RELAY_ON_EVT		EVT_BIT1
#define RELAY_OFF_EVT		EVT_BIT0

// Buzzer Event Bits
#define BUZZER_ON_EVT		EVT_BIT3
#define BUZZER_OFF_EVT		EVT_BIT2

static TaskHandle iot_remote_control_task_handle = NULL;
static EventGrpHandle iot_remote_control_evt_handle = NULL;

static IOT_REMOTE_CONTROL_CONFIGURATION_DATA app_config_data;

// IoT Remote Control Event Bits
EventBits ebits_in = (RELAY_ON_EVT | BUZZER_ON_EVT | RELAY_OFF_EVT | BUZZER_OFF_EVT);

static void check_and_set_relay_contol_event() {
	if (iot_remote_control_evt_handle != NULL) {
		// check Relay 1 Control status
		if (app_config_data.ctrl_relay_stat == 1) {
			// Generate RELAY_1_ON_EVT
			event_group_set_bits(iot_remote_control_evt_handle, RELAY_ON_EVT);
		} else if (app_config_data.ctrl_relay_stat == 0) {
			// Generate RELAY_1_OFF_EVT
			event_group_set_bits(iot_remote_control_evt_handle, RELAY_OFF_EVT);
		}

		// check Relay 2 Control status
		if (app_config_data.ctrl_buzzer_stat == 1) {
			// Generate RELAY_2_ON_EVT
			event_group_set_bits(iot_remote_control_evt_handle, BUZZER_ON_EVT);
		} else if (app_config_data.ctrl_buzzer_stat == 0) {
			// Generate RELAY_2_OFF_EVT
			event_group_set_bits(iot_remote_control_evt_handle, BUZZER_OFF_EVT);
		}
	}
}

static em_err app_configuration_process(char *app_config, size_t app_config_len) {
	em_err res = EM_FAIL;

	if (app_config != NULL) {
		EM_LOGW(TAG, "Received config: %s", app_config);

		res = parse_config_json(app_config, &app_config_data);
		if (res != EM_OK) {
			EM_LOGE(TAG, "Failed to parse App Configuration");
		} else {
			EM_LOGD(TAG, "RELAY: %d", app_config_data.ctrl_relay_stat);
			EM_LOGD(TAG, "BUZZER: %d", app_config_data.ctrl_buzzer_stat);
			check_and_set_relay_contol_event();
		}
	}
	return res;
}

static void iot_remote_control_task(void *param) {

	while (1) {
		if (iot_remote_control_evt_handle != NULL) {
			EventBits ebits_out = event_group_wait_bits(iot_remote_control_evt_handle, ebits_in, true, false,
					EventMaxDelay);

			if ((ebits_out & ebits_in) == RELAY_ON_EVT) {
				EM_LOGW(TAG, "Switching the Relay ON");
				set_gpio_value(RELAY_TOGGLE_GPIO, HIGH);
			} else if ((ebits_out & ebits_in) == RELAY_OFF_EVT) {
				EM_LOGW(TAG, "Switching the Relay OFF");
				set_gpio_value(RELAY_TOGGLE_GPIO, LOW);
			}

			if ((ebits_out & ebits_in) == BUZZER_ON_EVT) {
				EM_LOGW(TAG, "Switching the Buzzer ON");
				set_gpio_value(BUZZER_TOGGLE_GPIO, HIGH);
			} else if ((ebits_out & ebits_in) == BUZZER_OFF_EVT) {
				EM_LOGW(TAG, "Switching the Buzzer OFF");
				set_gpio_value(BUZZER_TOGGLE_GPIO, LOW);
			}
		} else {
			TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
		}
	}

	EM_LOGI(TAG, "Stop IoT Remote Control Task '%s'", __func__);
	iot_remote_control_task_handle = NULL;
	TaskDelete(NULL);
}

static void init_default_application_configuration() {
	app_config_data.ctrl_relay_stat = false;
	app_config_data.ctrl_buzzer_stat = false;

	TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);

	// by defaut the relay will be off.
	check_and_set_relay_contol_event();
}

void iot_remote_control_init() {
	/* Do all necessary initializations here */

	// Configure Relay GPIO as Output
	em_err res = configure_gpio(RELAY_TOGGLE_GPIO, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (res != EM_OK) {
		EM_LOGE(TAG, "Failed to configure Relay 1 Control GPIO");
	}

	// Configure Buzzer GPIO as Output
	res = configure_gpio(BUZZER_TOGGLE_GPIO, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (res != EM_OK) {
		EM_LOGE(TAG, "Failed to configure Relay 2 Control GPIO");
	}

	// Start the IoT Remote Control Task
	bool stat = TaskCreate(iot_remote_control_task, "iot_remote_ctrl_task", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_5,
			&iot_remote_control_task_handle);
	if (stat != true) {
		EM_LOGE(TAG, "Failed to start IoT Remote Control Task");
	}

	// Create an Event Group for the application
	iot_remote_control_evt_handle = event_group_create();
	if (iot_remote_control_evt_handle == NULL) {
		EM_LOGE(TAG, "Failed to Create IoT Remote Control Event handle");
	}

	// Register application configuration function with migCloud
	migcloud_register_app_config_function(app_configuration_process);

	// Set application's default configurations
	init_default_application_configuration();
}

void iot_remote_control_loop() {

}
